@include('layouts.partials.head')

<div class="hold-transition login-page">
  <div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="#" class="h1">
          <img src="{{asset('img/image.png')}}" alt="TSM Tracking Logo" style="width: 200px; height: px">
        </a>
      </div>
      <div class="card-body">

        <p class="login-box-msg">Sign in to start your session</p>

        @if(session('error'))<div class="alert alert-danger alert-dismissible fade show" role="alert">{{session('error')}}</div>@endif

        <form action="{{ route('login') }}" method="POST">

          @if(session('success'))<div class="alert alert-success mx-4 mt-2 alert-dismissible fade show" role="alert">{{session('success')}}</div>@endif
          @if ($errors->any())
            <div class="alert alert-danger mx-4 mt-2 alert-dismissible fade show" role="alert">
                <!-- <strong>Whoops!</strong> There were some problems with your input.<br><br> -->
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @csrf

          <div class="input-group mb-3">
            <input id="email" name="email" type="email" class="form-control" placeholder="Email" required autofocus>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required autocomplete="current-password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-8">
              <div class="icheck-primary">
                <p class="mb-1">
                    <a href="{{ route('password.request') }}">Forgot your password ?</a>
                  </p>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block">Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        {{-- <p class="mb-0">
          <a href="{{ route('register') }}" class="text-center">Create an account ?</a>
        </p> --}}
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
