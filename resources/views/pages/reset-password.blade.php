@include('layouts.partials.head')

<div class="hold-transition login-page">
  <div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="#" class="h1">
          <img src="{{asset('img/logo2.png')}}" alt="TSM Tracking Logo" style="width: 200px; height: px">
        </a>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Reset your password now!!</p>

        <form action="{{ route('password.update') }}" method="POST">
          @if(session('success'))<div class="alert alert-success mx-4 mt-2 alert-dismissible fade show" role="alert">{{session('success')}}</div>@endif
          @if ($errors->any())
            <div class="alert alert-danger mx-4 mt-2 alert-dismissible fade show" role="alert">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @csrf

          <!-- Password Reset Token -->
          <input type="hidden" name="token" value="{{ $request->route('token') }}">

          <div class="input-group mb-3">
            <input id="email" name="email" type="email" class="form-control" placeholder="Email" :value="old('email', $request->email)" required autofocus>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" placeholder="Confirm Password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="icheck-primary">
                <!-- <input type="checkbox" id="remember_me" name="remember"> -->
                <label for="remember_me">
                  <!-- Remember Me -->
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-6">
              <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
