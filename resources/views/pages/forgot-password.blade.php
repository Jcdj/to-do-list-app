@include('layouts.partials.head')

<div class="hold-transition login-page">
<div class="login-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
        <a href="#" class="h1">
          <img src="{{asset('img/image.png')}}" alt="TSM Tracking Logo" style="width: 200px; height: px">
        </a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.</p>

      <form action="{{ route('password.email') }}" method="POST">
        @if(session('success'))<div class="alert alert-success mx-4 mt-2 alert-dismissible fade show" role="alert">{{session('success')}}</div>@endif
        @if ($errors->any())
          <div class="alert alert-danger mx-4 mt-2 alert-dismissible fade show" role="alert">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

        @csrf

        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" name="email" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Request new password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mt-3 mb-1">
        <a href="{{ route('login') }}">Login</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</div>
