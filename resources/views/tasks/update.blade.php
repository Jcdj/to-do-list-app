


<div class="modal fade" id="edit-task">
    <div class="modal-dialog modal-dialog-scrollable">
      <div class="modal-content" action="{{ route('tasks.store') }}" method="post">

        <div class="modal-header">
          <h4 class="modal-title">Edit Tasks</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form id="u_task" class="modal-body" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" class="form-control" name="id" id="u_id" >
            <div class="card-body row">
              <div class="form-group col-md-6">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="u_title" placeholder="Title">
              </div>
              <div class="form-group col-md-12">
                <label for="description">Description</label>
                <input type="text" class="form-control" name="description" id="u_description" placeholder="Description">
              </div>
            </div>

        </form>

        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          <form  id="d_task" method="post">
            @csrf @method('DELETE')
          </form>
          <button onclick="deleteTask()" class="btn btn-danger">Delete</button>
          <button onclick="updateTask()" class="btn btn-primary">Update</button>
        </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- /.modal -->

<script>
function editTask(task) {
// console.log(task);
$("#u_active").on('change', function(){

  if(this.checked) $("#active").val("1");
  else $("#active").val("0");

});

$.each( task, function( key, value ) {
  // console.log("#u_%s : %s", key, value);

 if (key == "active") {
   $("#u_active").prop('checked', (value ? true : false));
   $("#active").val(value);
 }
 else $("#u_"+key).val(value).change();

});
$('#edit-task').modal('show');
}

function deleteTask() {
var form = $('#d_task');

var id = $('#u_id').val();
let _url     = `/tasks/${id}`;
form.attr('action', _url);
form.submit();
}

function updateTask() {
var form = $('#u_task');
var id = $('#u_id').val();
let _url     = `/tasks/${id}`;
form.attr('action', _url);
form.submit();

}
</script>
