@extends('layouts.adminlte', ['title' => 'Tasks List'])
@section('content')


<!-- Content Header (Page header) -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Tasks</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('tasks.index') }}">Tasks</a></li>
              <li class="breadcrumb-item active">List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">All Tasks</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#new-task">
                        New
                      </button>
                    </div>
                  </div>
                  @if(session('success'))<div class="alert alert-success mx-4 mt-2 alert-dismissible fade show" role="alert">{{session('success')}}</div>@endif
                  <!-- /.card-header -->
                  <div class="card-body">
                    @if ($errors->any())
                      <div class="alert alert-danger mx-4 mt-2 alert-dismissible fade show" role="alert">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                    @endif
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <!-- <th>Profile</th> -->
                          <th>Title</th>
                          <th>Description</th>
                          <th>Since</th>
                          <th>Permission</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($tasks as $key => $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <!-- <td>
                                  @if (isset($value->profile))
                                    <img class="img-circle elevation-2" width="30" src="{{ asset('storage/' . $value->profile) }}" alt="">
                                  @else
                                    <img class="img-circle elevation-2" width="30" src="{{ asset('/user.png') }}" alt="">
                                  @endif
                                </td> -->
                                <td>{{ $value->title }}</td>
                                <td>{{ $value->description }}</td>
                                <td>{{ $value->created_at }}</td>
                                <td>
                                  {{-- <button onclick='' type="button" class="btn btn-sm btn-success">Edit</button> --}}
                                  <button onclick='editTask(@json($value))' type="button" class="btn btn-sm btn-success">Edit</button>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                        <th>#</th>
                          <!-- <th>Profile</th> -->
                          <th>Title</th>
                          <th>Description</th>
                          <th>Since</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
        </div>
    </section>
    <!-- /.content -->

    @include('tasks.create')

    <!-- Update company -->
    @include('tasks.update')
    <!-- /.Update company -->

</div>
<!-- /.content-wrapper -->

@endsection
