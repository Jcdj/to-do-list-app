<div class="modal fade" id="new-task">
    <div class="modal-dialog modal-dialog-scrollable">
      <form class="modal-content" action="{{ route('tasks.store') }}" method="post" enctype="multipart/form-data">

        <div class="modal-header">
          <h4 class="modal-title">New Tasks</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            @csrf
            <div class="card-body row">
              <div class="form-group col-md-6">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Title">
              </div>
              <div class="form-group col-md-12">
                <label for="description">Description</label>
                <input type="text" class="form-control" name="description" id="description" placeholder="Description">
              </div>
            </div>
        </div>

        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>

      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- /.modal -->
