<!DOCTYPE html>
<html lang="en">
<head>
  <title>{{-- {{ env('APP_NAME') }} | --}} {{ $title }}</title>
  <link rel="icon" type="image/png" href="{{asset('img/logob.png')}}"/>
  @include('layouts.partials.head')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{asset('img/image.png')}}" alt="UserLogo" height="240" width="240">
  </div>

  <!-- Navbar -->
  @include('layouts.partials.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a class="brand-link">
      <img src="{{asset('img/logob.png')}}" alt="Memo App Logo" class="brand-image img-circle elevation-1" style="opacity: ">
      <span class="brand-text font-weight-light" translate="no">{!! env('APP_NAME') !!}</span>
    </a>

    <!-- Sidebar -->
    @include('layouts.partials.sidebar')
    <!-- /.sidebar -->
  </aside>


  @yield('content')


  @include('layouts.partials.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('layouts.partials.scripts')

</body>
</html>
