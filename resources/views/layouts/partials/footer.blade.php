<footer class="main-footer"  translate="no">
    <strong>Copyright &copy; 2022-2023 <a href="">{{ env('APP_NAME') }}</a>.</strong>
    All rights reserved.
</footer>
