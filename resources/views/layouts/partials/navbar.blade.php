<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto" right="10">
      <li class="nav-item dropdown">
        <form method="POST" action="{{ route('logout') }}">
          @csrf
          <a class="nav-link" data-toggle="dropdown" href="#">
            @if (isset(Auth::user()->profile))
              <img src="{{ asset('storage/' . Auth::user()->profile) }}" alt="" class="img-circle elevation-2" width="30">
            @else
              <div style="display: flex; align-items: center;">
                <img src="{{asset('/user.png')}}" alt="" class="img-circle elevation-2" width="30"  style="margin-top: -6px; margin-right: 5px">
                {{-- <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" width="30">
                  <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                </svg> --}}
                <strong>
                  {{ Auth::user()->name }}
                </strong>
              </div>
            @endif
          </a>

          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="{{route('profile.edit')}}">
                {{-- <strong>
                  {{ Auth::user()->name }}
                </strong> --}}
                <i class="fa fa-user mr-1"></i>
                <p class="d-none d-md-inline d-lg-inline">Profile</p>
            </a>
            <a onclick="event.preventDefault();this.closest('form').submit();" href="{{route('logout')}}" class="dropdown-item">
                <i class="fa fa-power-off mr-1"></i>
                <span class="d-none d-md-inline d-lg-inline">Logout</span>
            </a>
          </div>

        </form>
      </li>
      {{-- <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li> --}}
    </ul>
  </nav>
