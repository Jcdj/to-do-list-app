@extends('layouts.adminlte', ['title' => 'Tasks List'])
@section('content')


<!-- Content Header (Page header) -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper mb-4">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Profile</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  @vite(['resources/css/app.css', 'resources/js/app.js'])
  <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg mb-4">
                    <div class="max-w-xl">
                        @include('profile.partials.update-profile-information-form')
                    </div>
                </div>

                <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                    <div class="max-w-xl">
                        @include('profile.partials.update-password-form')
                    </div>
                </div>
              </div>
            </div>
        </div>
    </section>

</div>

@endsection
