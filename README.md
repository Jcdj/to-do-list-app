# To Do List App

## Clone the project
To use the app locally, you have to firstly clone the project using the command:
    $ git clone https://gitlab.com/Jcdj/to-do-list-app.git 

And then install the dependencies using these commands:
    $ composer install
    $ npm install

## DB and Tables
After cloning the project:
    Create the .env file based on the .env.example 
    Generate a key if necessary
    Create a local DB using the following infos in :
        DB_DATABASE=to-do-list-app
        DB_USERNAME=root
        DB_PASSWORD=

Now, run the migrations and the seeders using the command:
    $ php artisan migrate:fresh --seed

## Run the App
To run the application, use the command:
    $ php artisan serve

## Use the App
to use the app, firstly click on the link provided by the previous command.
On the login page, use the following credentials for the admin user:
    email : admin@admin.com
    password : password
And then, you will be redirected to the Tasks view where you can:
    Execute the CRUD oporations for a tasks
    Search a specific task
    Download/print the task list 

Also, you can edit the admin information by clinking on the user icon in the navbar and then on Profile.

To logout the session, click on the user icon in the navbar and then on Logout.

## Docker
To launch the app using Docker
    Take care of installing Docker Desktop using the official Doc.
    Build the container using the command:
        $ docker-compose up --build -d
    Next, on Docker, open the erminal of the service web-1 and execute the command:
        $ php artisan migrate:fresh --seed
    Now, you can launch the app either by clinking on 8080:80 in Docker Desktop or by typing http://localhost:8080/ in the browser
